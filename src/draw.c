// Code reference: Juan Gallostra Acín: https://github.com/juangallostra/game-of-life

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>

#include "life.h"

void draw(int *pWorld, int maxRows, int maxColumns, int speed)
{
	initscr();
	noecho();
	erase();
	curs_set(FALSE);
	for (int y = 0; y < maxRows; y++)
	{
		for (int x = 0; x < maxColumns; x++)
		{
			if (*(pWorld + y * maxRows + x) == ALIVE)
			{
				mvaddch(y, x, ACS_CKBOARD);
			}
			if(x == 0 || y == 0 || x == maxColumns - 1 || y == maxRows - 1){
			    mvaddch(y, x, ACS_CKBOARD);
			}
		}
	}
	refresh();
	usleep(speed);
}

void drawToFile(int *pWorld, int maxRows, int maxColumns, unsigned char* outputFileName)
{
    remove(outputFileName);
    FILE*f=fopen(outputFileName,"w");
    for (int y = 0; y < maxRows; y++)
    {
        for (int x = 0; x < maxColumns; x++)
        {
            int cellIndex = y * maxRows + x;
            if(*(pWorld+cellIndex)==ALIVE)
     		{
     		    fprintf(f, "+");
     		}
     		else{
     			fprintf(f, " ");
     		}

     	}
     	fprintf(f, "\n");
    }
    fclose(f);
}
