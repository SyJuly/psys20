#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/times.h>
#include <time.h>
#include <unistd.h>

#include "evolve.h"
#include "draw.h"
#include "life.h"
#include "input.h"

void runGameOfLife(int *pWorldBufferA, int maxRows, int maxColumns,  int generations, int speed);
struct exec_time runGameOfLifeWithoutDraw(int *pWorldBufferA, int maxRows, int maxColumns, int generations);

int ALIVE = 1;
int DEAD = 0;

struct exec_time{
    long double usr;
    long double sys;
    long double real;
};

int main(int argc, char **argv)
{
    // REQUIRED ARGS
    if(argc < 3){
        printf("Please provide the world dimensions as follows: \"./gameOfLife numberOfRows numberOfColumns\" \nCanceling Conway's Game.\n");
        return 0;
    }
    int maxRows = atoi(argv[1]);
    int maxColumns = atoi(argv[2]);
    if(maxRows < 2 || maxColumns < 2 ){
            printf("Please provide world dimensions that are greater than 2 x 2. \nCanceling Conway's Game.\n");
            return 0;
    }

    int opt;

    //  OPTIONAL ARGS, DEFAULT VALUES
    int generations = 100;
    int repetitions = 1;
    int drawingMode = 0;
    int drawingMicroSecPerGen = 1000000;
    unsigned char* inputFileName = "../input/input.bmp";
    unsigned char* outputFileName = "../output/gameOfLifeOutput.txt";

    printf("Conway's game of life.\n");
    printf("-----------------------------------\n");
    printf("Configuration: \n\n");

    while((opt = getopt(argc, argv, "g:r:ds:i:o:")) != -1)
        {
            switch(opt)
            {
                case 'g':
                    generations = atoi(optarg);
                    break;
                case 'r':
                    repetitions = atoi(optarg);
                    break;
                case 'd':
                    drawingMode = 1;
                    break;
                case 's':
                    drawingMicroSecPerGen = (int)(atof(optarg) * 1000000);
                    break;
                case 'i':
                    inputFileName = optarg;
                    break;
                case 'o':
                    outputFileName = optarg;
                    break;
                case ':':
                    printf("Option needs a value\n");
                    break;
                case '?':
                    printf("unknown option: %c\n", optopt);
                    break;
            }
        }
    if(drawingMode == 1){
        printf("    Drawing mode ON.\n");
        printf("    Drawing one generation every %d\n", drawingMicroSecPerGen);
    } else {
        printf("    Drawing mode OFF.\n");
    }
    printf("    World size: %dx", maxRows);
    printf("%d\n", maxColumns);
    printf("    Generations: %d\n", generations);
    printf("    Repetitions: %d\n", repetitions);
    printf("    Input file: %s\n", inputFileName);
    printf("    Output file: %s\n", outputFileName);
    printf("-----------------------------------\n");


    int * pWorldBufferA = calloc(maxRows * maxColumns, sizeof(int));

    writeInitialPopulation(pWorldBufferA, maxRows, maxColumns, inputFileName);

    if(drawingMode == 1){
        runGameOfLife(pWorldBufferA, maxRows, maxColumns, generations, drawingMicroSecPerGen);
    } else {
        struct exec_time sum_times = {.usr = 0.0, .sys = 0.0, .real = 0.0};
        for(int i = 0; i < repetitions; i++){
            struct exec_time time = runGameOfLifeWithoutDraw(pWorldBufferA, maxRows, maxColumns, generations);
            sum_times.real += time.real;
            sum_times.usr += time.usr;
            sum_times.sys += time.sys;
        }
        printf("\n......................\n\nAverage real time: %Lf\n",sum_times.real / repetitions);
        printf("Average usr time: %Lf\n", sum_times.usr / repetitions);
        printf("Average sys time: %Lf\n", sum_times.sys / repetitions);
    }
    drawToFile(pWorldBufferA,  maxRows,  maxColumns, outputFileName);
	return 0;
}

struct exec_time runGameOfLifeWithoutDraw(int *pWorldBufferA, int maxRows, int maxColumns, int generations){
    struct exec_time exec_times = {.usr = 0.0, .sys = 0.0, .real = 0.0};
    struct tms start_t, end_t;
    clock_t start, end;
    int tics_per_second = sysconf(_SC_CLK_TCK);

    start = times(&start_t);

    int currentGeneration = 0;

    int *pWorldBufferB = calloc(maxRows * maxColumns, sizeof(*pWorldBufferA));

    while(currentGeneration < generations)
    {
        int * pInputWorld = currentGeneration%2 == 0 ? pWorldBufferA : pWorldBufferB;
        int * pOutputWorld = currentGeneration%2 == 0 ? pWorldBufferB : pWorldBufferA;
        evolve(pInputWorld, pOutputWorld, maxRows, maxColumns);
        currentGeneration++;
	}

    end = times(&end_t);
	exec_times.real = ((long double)end - start)/ tics_per_second;
	exec_times.usr = ((long double) end_t.tms_utime - start_t.tms_utime)/tics_per_second;
	exec_times.sys = ((long double) end_t.tms_stime - start_t.tms_stime)/tics_per_second;

	int aliveCells = 0;
    for (int y = 1; y < maxRows - 1; y++)
	{
		for (int x = 1; x < maxColumns - 1; x++)
		{
            if (pWorldBufferA[y * maxRows + x] == ALIVE)
			{
				aliveCells++;
			}
		}
	}
    printf("\nFinished game of life after %Lf seconds for %d generations.\n", exec_times.real, generations);
    printf("Alive cells in given world: %d\n", aliveCells);

    return exec_times;
}

void runGameOfLife(int *pWorldBufferA, int maxRows, int maxColumns, int generations, int speed){
    int currentGeneration = 0;
    int *pWorldBufferB = calloc(maxRows * maxColumns, sizeof(*pWorldBufferA));
    while(currentGeneration < generations)
    {
        int * pInputWorld = currentGeneration%2 == 0 ? pWorldBufferA : pWorldBufferB;
        int * pOutputWorld = currentGeneration%2 == 0 ? pWorldBufferB : pWorldBufferA;
        draw(pInputWorld, maxRows, maxColumns, speed);
        evolve(pInputWorld, pOutputWorld, maxRows, maxColumns);
        currentGeneration++;
    }
}