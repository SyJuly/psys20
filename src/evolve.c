#include <stdio.h>
#include <stdlib.h>

#include "life.h"

const int NEIGHBOURS_PER_CELL = 8;

const int neighbours[8][2] = {{-1,-1},
                        {-1, 0},
                        {-1, 1},
                        {0, -1},
                        {0,  1},
                        {1, -1},
                        {1,  0},
                        {1,  1}};

int decideLife(int livingNeighbours, int livingStatus)
{
    /*
      Rules of Conway's Game of life: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

    */
	if (livingStatus == ALIVE && (livingNeighbours == 2 || livingNeighbours == 3))
	{
	    // Any live cell with two or three live neighbors survives
		return ALIVE;
	} else if (livingStatus == DEAD && livingNeighbours == 3)
	{
	    // Any dead cell with three live neighbors becomes a live cell.
		return ALIVE;
	}

	//All other live cells die in the next generation. Similarly, all other dead cells stay dead.
	return DEAD;
}


void evolve(int *pInputWorld, int *pOutputWorld, int maxRows, int maxColumns)
{

    // limited by world definition, can't access all neighbours from index 0
	for (int y = 1; y < maxRows - 1; y++)
	{
		for (int x = 1; x < maxColumns - 1; x++)
		{
		    int cellIndex = y * maxColumns + x;
			int livingNeighboursPerCell = 0;

			for (int n = 0; n < NEIGHBOURS_PER_CELL; n++)
			{
			    livingNeighboursPerCell += pInputWorld[(y + neighbours[n][0]) * maxColumns + x + neighbours[n][1]];
			}
			pOutputWorld[cellIndex] = decideLife(livingNeighboursPerCell, pInputWorld[cellIndex]);
		}
	}
}