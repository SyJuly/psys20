/**
 * Test for functions in input.c
 *
 */
#include <assert.h>
#include <stdio.h>
#include "input.c"

int ALIVE = 1;
int DEAD = 0;

struct testConfig{
    int maxRows;
    int maxColumns;
    int inputWidth;
    int inputHeight;
};

struct testResult{
    int blackPixels;
    int aliveCells;
};

void test_writeInputIntoWorld_withEmptyInput(void);
void test_writeInputIntoWorld_withSmallerInputThanWorld(void);
void test_writeInputIntoWorld_withHigherInputThanWorld(void);
void test_writeInputIntoWorld_withFlatWorld(void);

struct testResult test_writeInputIntoWorld(struct testConfig testConfig, unsigned char* input);
unsigned char* generateInput(int inputWidth, int inputHeight, int color);


void test_writeInputIntoWorld_withEmptyInput()
{
    struct testConfig config ={
        .maxRows = 10,
        .maxColumns = 10,
        .inputWidth = 10,
        .inputHeight = 10,
    };

    unsigned char* input = generateInput(config.inputWidth, config.inputHeight, 255);

    struct testResult result = test_writeInputIntoWorld(config, input);
    assert(result.blackPixels == result.aliveCells);
}

void test_writeInputIntoWorld_withSmallerInputThanWorld()
{
    struct testConfig config ={
        .maxRows = 10,
        .maxColumns = 10,
        .inputWidth = 9,
        .inputHeight = 9,
    };

    unsigned char* input = generateInput(config.inputWidth, config.inputHeight, 0);

    struct testResult result = test_writeInputIntoWorld(config, input);
    assert(result.blackPixels == result.aliveCells);
}

void test_writeInputIntoWorld_withHigherInputThanWorld()
{
    struct testConfig config ={
        .maxRows = 10,
        .maxColumns = 10,
        .inputWidth = 15,
        .inputHeight = 15,
    };

    unsigned char* input = generateInput(config.inputWidth, config.inputHeight, 0);

    int numberOfCells = config.maxColumns * config.maxRows;
    int numberOfPixels = config.inputHeight * config.inputWidth;
    int diff =  numberOfPixels - numberOfCells;

    struct testResult result = test_writeInputIntoWorld(config, input);

    assert( result.aliveCells == config.maxColumns * config.maxRows );
    assert( result.aliveCells == (numberOfPixels - diff) );
}

void test_writeInputIntoWorld_withFlatWorld()
{
    struct testConfig config ={
        .maxRows = 4,
        .maxColumns = 10,
        .inputWidth = 15,
        .inputHeight = 15,
    };

    unsigned char* input = generateInput(inputWidth, inputHeight, 0);

    int numberOfCells = config.maxColumns * config.maxRows;
    int numberOfPixels = config.inputHeight * config.inputWidth;
    int diff =  numberOfPixels - numberOfCells;

    struct testResult result = test_writeInputIntoWorld(config, input);

    assert( result.aliveCells == (numberOfPixels - diff) );
    assert( result.aliveCells == (numberOfPixels - diff) );

}


struct testResult test_writeInputIntoWorld(struct testConfig config, unsigned char* input)
{
    struct testResult result = {
        .blackPixels = 0,
        .aliveCells = 0,
    };

    int counter = 0;

    for(int i = 0; i < config.inputHeight; i++)
    {
        for(int j = 0; j < config.inputWidth; j++)
        {
            int value = input[3 * (i * config.inputWidth + j)]; // taking blue spectrum as black indicator
            if(value == 0)
            {
                result.blackPixels++;
            }
        }
    }

    int * world = calloc(config.maxRows * config.maxColumns, sizeof(int));

    writeInputIntoWorld(world, config.maxRows, config.maxColumns, config.inputWidth, config.inputHeight, input);

    for(int y = 0; y < config.maxRows; y++)
    {
        for(int x = 0; x < config.maxColumns; x++)
        {
            if(world[y * config.maxColumns + x] == ALIVE)
            {
                result.aliveCells++;
            }
        }
    }
    //printf("\nnumber of black pixels: %d and number of alive cells: %d-----", result.blackPixels, result.aliveCells);
    return result;

}



unsigned char* generateInput(int inputWidth, int inputHeight, int color)
{
    unsigned char* input = realloc(input, 3 * inputWidth * inputHeight * sizeof(unsigned char));
    for(int i = 0; i < inputHeight; i++)
    {
        for(int j = 0; j < inputWidth; j++)
        {
            input[3 * (i * inputWidth + j)] = color;
            input[3 * (i * inputWidth + j) + 1] = color;
            input[3 * (i * inputWidth + j) + 2] = color;
        }
    }
    return input;
}


/**
 * Main entry for the test.
 */
int main(int argc, char **argv)
{
    printf("\n\n----- Testing world initialisation with input -----\n");
    test_writeInputIntoWorld_withEmptyInput();
    test_writeInputIntoWorld_withSmallerInputThanWorld();
    test_writeInputIntoWorld_withHigherInputThanWorld();
    test_writeInputIntoWorld_withFlatWorld();
	return 0;
}


