#ifndef INPUT_H_
#define INPUT_H_

void writeInitialPopulation(int *pWorld, int maxRows, int maxColumns, unsigned char* filename);
void writeInputIntoWorld(int *pWorld, int maxRows, int maxColumns, int inputWidth, int inputHeight, unsigned char* input);
unsigned char* readBMP(unsigned char* filename);

#endif