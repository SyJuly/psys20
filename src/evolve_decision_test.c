/**
 * Test for functions in evolve.c
 *
 */
#include <assert.h>
#include <stdio.h>
#include "evolve.c"

int ALIVE = 1;
int DEAD = 0;

void test_decideLife_deadStaysDead(void);
void test_decideLife_aliveStaysAlive(void);
void test_decideLife_aliveDiesFromOverpopulation(void);
void test_decideLife_aliveDiesFromUnderpopulation(void);
void test_decideLife_deadBecomesAlive(void);

void test_decideLife_deadBecomesAlive(){
    int cellStatus = decideLife(3, DEAD);
    assert(cellStatus == ALIVE);
}

void test_decideLife_aliveStaysAlive(){
    int cellStatus = decideLife(3, ALIVE);
    assert(cellStatus == ALIVE);

    cellStatus = decideLife(2, ALIVE);
    assert(cellStatus == ALIVE);
}

void test_decideLife_aliveDiesFromOverpopulation(){
    int cellStatus = decideLife(4, ALIVE);
    assert(cellStatus == DEAD);
}

void test_decideLife_aliveDiesFromUnderpopulation(){
    int cellStatus = decideLife(1, ALIVE);
    assert(cellStatus == DEAD);

    cellStatus = decideLife(0, ALIVE);
    assert(cellStatus == DEAD);
}

void test_decideLife_deadStaysDead(){
    int cellStatus = decideLife(0, DEAD);
    assert(cellStatus == DEAD);
}

/**
 * Main entry for the test.
 */
int main(int argc, char **argv)
{
    printf("\n\n----- Testing evolution decision -----\n");
    test_decideLife_deadStaysDead();
    test_decideLife_aliveStaysAlive();
    test_decideLife_aliveDiesFromOverpopulation();
    test_decideLife_aliveDiesFromUnderpopulation();
    test_decideLife_deadBecomesAlive();
	return 0;
}