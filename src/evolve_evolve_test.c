/**
 * Test for functions in evolve.c
 *
 */
#include <assert.h>
#include <stdio.h>
#include "evolve.c"

int ALIVE = 1;
int DEAD = 0;

void test_evolve(void);
void test_evolve_isDeterministic(void);
void test_evolve_WithRectangleWorld(void);
int * evolve_Acorn(int maxColumns, int maxRows);

void test_evolve_gliderResult(){
    int maxColumns = 7;
    int maxRows = 8;
    int iterations = 10;
    int * worldBufferA = calloc(maxRows * maxColumns, sizeof(int));
    int * worldBufferB = calloc(maxRows * maxColumns, sizeof(int));
    int * worldResult = calloc(maxRows * maxColumns, sizeof(int));

    int * inputWorld = worldBufferA;
    int * outputWorld = worldBufferB;

    //glider pattern
    inputWorld[1 * maxColumns + 2] = ALIVE;
    inputWorld[2 * maxColumns + 3] = ALIVE;
    inputWorld[3 * maxColumns + 1] = ALIVE;
    inputWorld[3 * maxColumns + 2] = ALIVE;
    inputWorld[3 * maxColumns + 3] = ALIVE;

    //glider result pattern after 10 iterations
    worldResult[4 * maxColumns + 5] = ALIVE;
    worldResult[5 * maxColumns + 3] = ALIVE;
    worldResult[5 * maxColumns + 5] = ALIVE;
    worldResult[6 * maxColumns + 4] = ALIVE;
    worldResult[6 * maxColumns + 5] = ALIVE;


    for(int i = 0; i < iterations; i++){
        inputWorld = i%2 == 0 ? worldBufferA : worldBufferB;
        outputWorld = i%2 == 0 ? worldBufferB : worldBufferA;
        evolve(inputWorld, outputWorld, maxRows, maxColumns);
    }

    for (int y = 0; y < maxRows; y++)
	{
		for (int x = 0; x < maxColumns; x++)
		{
		    int cellIndex = y * maxColumns + x;
		    assert(outputWorld[cellIndex] == worldResult[cellIndex]);
		}
    }
}


void test_evolve_WithRectangleWorld()
{
    int maxColumns = 100;
    int maxRows = 30;

    int iterations = 50;
    int * worldBufferA = calloc(maxRows * maxColumns, sizeof(int));
    int * worldBufferB = calloc(maxRows * maxColumns, sizeof(int));

    int * inputWorld = worldBufferA;
    int * outputWorld = worldBufferB;

    //small exploder pattern
    inputWorld[12 * maxColumns + 45] = ALIVE;
    inputWorld[13 * maxColumns + 44] = ALIVE;
    inputWorld[13 * maxColumns + 45] = ALIVE;
    inputWorld[13 * maxColumns + 46] = ALIVE;
    inputWorld[14 * maxColumns + 44] = ALIVE;
    inputWorld[14 * maxColumns + 46] = ALIVE;
    inputWorld[15 * maxColumns + 45] = ALIVE;

    for(int i = 0; i < iterations; i++){
        inputWorld = i%2 == 0 ? worldBufferA : worldBufferB;
        outputWorld = i%2 == 0 ? worldBufferB : worldBufferA;
        evolve(inputWorld, outputWorld, maxRows, maxColumns);
    }

    int aliveCellCounter = 0;
    for (int y = 0; y < maxRows; y++)
	{
		for (int x = 0; x < maxColumns; x++)
		{
		    int cellIndex = y * maxColumns + x;
		    if(outputWorld[cellIndex] == ALIVE){
		        aliveCellCounter++;
		    }
		}
    }
    assert(aliveCellCounter == 6 * 4);
}

void test_evolve_isDeterministic()
{
    int maxColumns = 100;
    int maxRows = 100;
    int * worldFirstTry = evolve_Acorn(maxColumns, maxRows);
    int * worldSecondTry = evolve_Acorn(maxColumns, maxRows);

    for (int y = 0; y < maxRows; y++)
	{
		for (int x = 0; x < maxColumns; x++)
		{
		    int cellIndex = y * maxColumns + x;
		    assert(worldFirstTry[cellIndex] == worldSecondTry[cellIndex]);
		}
    }
}

int * evolve_Acorn(int maxColumns, int maxRows)
{
    int iterations = 100;
    int * worldBufferA = calloc(maxRows * maxColumns, sizeof(int));
    int * worldBufferB = calloc(maxRows * maxColumns, sizeof(int));

    int * inputWorld = worldBufferA;
    int * outputWorld = worldBufferB;

    //acorn pattern
    inputWorld[21 * maxColumns + 86] = ALIVE;
    inputWorld[20 * maxColumns + 84] = ALIVE;
    inputWorld[22 * maxColumns + 84] = ALIVE;
    inputWorld[22 * maxColumns + 83] = ALIVE;
    inputWorld[22 * maxColumns + 87] = ALIVE;
    inputWorld[22 * maxColumns + 88] = ALIVE;
    inputWorld[22 * maxColumns + 89] = ALIVE;

    for(int i = 0; i < iterations; i++){
        inputWorld = i%2 == 0 ? worldBufferA : worldBufferB;
        outputWorld = i%2 == 0 ? worldBufferB : worldBufferA;
        evolve(inputWorld, outputWorld, maxRows, maxColumns);
    }

    return outputWorld;
}

/**
 * Main entry for the test.
 */
int main(int argc, char **argv)
{
    printf("\n\n----- Testing evolution -----\n");
    test_evolve_gliderResult();
    test_evolve_isDeterministic();
    test_evolve_WithRectangleWorld();
	return 0;
}