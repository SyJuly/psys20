#ifndef DRAW_H_
#define DRAW_H_

void draw(int *pWorld, int maxRows, int maxColumns, int speed);
void drawToFile(int *pWorld, int maxRows, int maxColumns, unsigned char* outputFileName);

#endif