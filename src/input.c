#include <stdio.h>
#include <stdlib.h>

#include "input.h"
#include "life.h"

int BLACK = 0;
int COLORS = 3; //rgb
int inputWidth;
int inputHeight;

/*
    The initial setup for the alive cells are read from an BMP-File,
    in which the black pixel symbolize the living cells.
*/

void writeInitialPopulation(int *pWorld, int maxRows, int maxColumns, unsigned char* filename)
{
    printf("Reading input file.\n");
    unsigned char* input = readBMP(filename);
    printf("Converting %dx%d pixels.\n",inputWidth, inputHeight);

    writeInputIntoWorld(pWorld, maxRows, maxColumns, inputWidth, inputHeight, input);

}

void writeInputIntoWorld(int *pWorld, int maxRows, int maxColumns, int inputWidth, int inputHeight, unsigned char* input)
{
    int worldCenterX = maxColumns / 2;
	int worldCenterY = maxRows / 2;
    int inputCenterX = inputWidth / 2;
    int inputCenterY = inputHeight / 2;

    int numAliveCells = 0;

    for(int i = 0; i < inputHeight; i++)
    {
        for(int j = 0; j < inputWidth; j++)
        {
            int value = input[COLORS * (i * inputWidth + j)]; // taking blue spectrum as black indicator
            if(value == BLACK)
            {
                numAliveCells++;
                int y = worldCenterY - inputCenterY + i;
                y = y < 0 ? 0 : (y > maxRows - 1 ? maxRows - 1 : y);

                int x = worldCenterX - inputCenterX + j;
                x = x < 0 ? 0 : (x > maxColumns -1 ? maxColumns - 1 : x);

                pWorld[y * maxColumns + x] = ALIVE;
            }

        }
    }
    printf("Found %d alive cells.\n",numAliveCells);
}

unsigned char* readBMP(unsigned char* filename)
{
    //Reference: https://stackoverflow.com/questions/9296059/read-pixel-value-in-bmp-file (accessed on 20/05/2020)

    FILE* f = fopen(filename, "rb");
    unsigned char info[54];

    // read the 54-byte header
    fread(info, sizeof(unsigned char), 54, f);

    // extract image height and width from header
    inputWidth = *(int*)&info[18];
    inputHeight = *(int*)&info[22];

    // allocate 3 bytes per pixel
    int size = 3 * inputWidth * inputHeight;
    unsigned char* data = malloc(size*sizeof(unsigned char));;

    // read the rest of the data at once
    fread(data, sizeof(unsigned char), size, f);
    fclose(f);

    /*

    Colors stored as B, G, R, transition to R, G, B as follows

    for(int i = 0; i < size; i += 3)
    {
            // flip the order of every 3 bytes
            unsigned char tmp = data[i];
            data[i] = data[i+2];
            data[i+2] = tmp;
    }*/
    return data;
}