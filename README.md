# Conway's Game of Life

## How to play
`make`

`./game_of_life row_size column_size`

Example:

`./game_of_life 50 50`

### Customize Configuration (OPTIONAL)

`-g | set number of generations for the game of life (default: 100)`

`-d | turn drawing mode ON, no value required (default: OFF)`

`-s | set seconds per generation in drawing mode (default: 1s)`

`-i | set another file path for the input BMP-Image (default: "../input/input.bmp")`

`-o | set another file path for the output TXT (default: "../output/gameOfLifeOutput.txt")`

`-r | set number of repetitions to measure average time (default: 1)`

Examples:

`./game_of_life 50 50 -d -s 2 -g 200 -i "../input/input3.bmp" -o "../output/output.txt"`

`./game_of_life 50 50 -g 10000`


#### Input

The default input data is gathered from the Input-Folder: *"../input/input.bmp"*

You can customize the input data by changing the BMP-Image or provide another file path. The Input-File has to be a BMP-Image (black/white) where the living cells are black-colored pixels. The data from the Input-Image will get placed in the center of the world, so the Image-File dimensions have to be the same or smaller than the world dimensions.




